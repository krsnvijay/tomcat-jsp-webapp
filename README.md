# Tomcat + JSP Servlet
Clone repository https://gitlab.com/krsnvijay/tomcat-jsp-webapp.git to a folder, let's call it `yourproject/`

Go to `yourproject/` in terminal

## Gradle
Install gradle from https://gradle.org/install/
### Linux
```bash
$ ./gradlew tomcatRun
```
### Windows
```bash
$ gradlew tomcatRun
```
## Docker
Install Docker from https://docs.docker.com/install/
### Build Docker Image
```bash
$ sudo docker image build -t webapp:1.0
```
### Run Docker Image
```bash
$ sudo docker container run webapp:1.0
```
### To Find Container Id
```bash
$ sudo docker container ls
```
### To Find Container IpAddress
```bash
$ sudo docker inspect <CONTAINER_ID> | grep "IPAddress"
```
>>>
Visit `http://localhost:8080/tomcat-jsp-webapp` or `http://<IPAddress>:8080/tomcat-jsp-webapp` to see if your website is running!
>>>