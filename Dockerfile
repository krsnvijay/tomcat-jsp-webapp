FROM gradle:alpine as builder
ADD --chown=gradle . /home/gradle/tomcat-jsp-webapp
RUN gradle -p=tomcat-jsp-webapp war

FROM tomcat:alpine
COPY --from=builder /home/gradle/tomcat-jsp-webapp/build/libs/tomcat-jsp-webapp.war /usr/local/tomcat/webapps/
CMD ["catalina.sh", "run"]